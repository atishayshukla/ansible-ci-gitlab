### The goal of this is to become familiar with the gitlab ci cd solution

1. A lot can be gained by looking at the build yaml file.

2. It is particularly important to take note of quotes and all as it will give a lot of syntax error otherwise.

3. There are 3 stages and if you make 2 runs in the same stage gitlab will run it parallely which is great for automation tests.

4. Each run and the stage is run in a separate container therefore design it correctly.

5. only master is optional and it is also the default

6. Have to explore more.